package book.library.service;

import java.util.List;

import book.library.model.Author;

public interface AuthorService {

    void loadAuthors();

    List<Author> getAuthors();
}
