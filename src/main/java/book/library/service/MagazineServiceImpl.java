package book.library.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import book.library.model.Magazine;
import com.ibm.icu.text.CharsetDetector;
import com.ibm.icu.text.CharsetMatch;
import com.univocity.parsers.common.processor.BeanListProcessor;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.text.Collator.getInstance;
import static java.util.Collections.emptyList;
import static java.util.Comparator.comparing;
import static java.util.Locale.GERMAN;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

public class MagazineServiceImpl implements MagazineService {

    private List<Magazine> magazines = emptyList();

    @Override
    public void loadMagazines() {
        String charset = null;
        final String fileName = "magazines.csv";

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(fileName)) {
            final CharsetDetector charsetDetector = new CharsetDetector();
            charsetDetector.setText(inputStream);
            final CharsetMatch charsetMatch = charsetDetector.detect();
            charset = charsetMatch != null ? charsetMatch.getName() : UTF_8.name();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        final BeanListProcessor<Magazine> rowProcessor = new BeanListProcessor<>(Magazine.class);
        final CsvParserSettings parserSettings = new CsvParserSettings();
        parserSettings.setProcessor(rowProcessor);
        parserSettings.setHeaderExtractionEnabled(true);
        parserSettings.setDelimiterDetectionEnabled(true, ';');
        final CsvParser parser = new CsvParser(parserSettings);
        parser.parse(getClass().getClassLoader().getResourceAsStream(fileName), charset);

        // The BeanListProcessor provides a list of objects extracted from the input.
        magazines = rowProcessor.getBeans();
    }

    @Override
    public List<Magazine> geMagazines() {
        return magazines;
    }

    @Override
    public Set<Magazine> findByIsbn(final String isbn) {
        return magazines.stream().filter(magazine -> magazine.getIsbn().equals(isbn)).collect(toSet());
    }

    @Override
    public Set<Magazine> findByAuthor(final String author) {
        final Set<Magazine> foundMagazines = new HashSet<>();

        for (Magazine magazine : magazines) {
            for (String authorInAMagazine : magazine.getAuthors()) {
                if (authorInAMagazine.equals(author)) {
                    foundMagazines.add(magazine);
                }
            }
        }

        return foundMagazines;
    }

    @Override
    public List<Magazine> findAllMagazinesSortByGermanTitle() {
        return magazines.stream().sorted(comparing(Magazine::getTitle, getInstance(GERMAN))).collect(toList());
    }
}
