package book.library.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import book.library.model.Author;
import com.ibm.icu.text.CharsetDetector;
import com.ibm.icu.text.CharsetMatch;
import com.univocity.parsers.common.processor.BeanListProcessor;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Collections.emptyList;

public class AuthorServiceImpl implements AuthorService {

    private List<Author> authors = emptyList();

    @Override
    public void loadAuthors() {
        String charset = null;
        final String fileName = "authors.csv";

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(fileName)) {
            final CharsetDetector charsetDetector = new CharsetDetector();
            charsetDetector.setText(inputStream);
            final CharsetMatch charsetMatch = charsetDetector.detect();
            charset = charsetMatch != null ? charsetMatch.getName() : UTF_8.name();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        final BeanListProcessor<Author> rowProcessor = new BeanListProcessor<>(Author.class);
        final CsvParserSettings parserSettings = new CsvParserSettings();
        parserSettings.setProcessor(rowProcessor);
        parserSettings.setHeaderExtractionEnabled(true);
        parserSettings.setDelimiterDetectionEnabled(true, ';');
        final CsvParser parser = new CsvParser(parserSettings);
        parser.parse(getClass().getClassLoader().getResourceAsStream(fileName), charset);

        // The BeanListProcessor provides a list of objects extracted from the input.
        authors = rowProcessor.getBeans();
    }

    @Override
    public List<Author> getAuthors() {
        return authors;
    }
}
