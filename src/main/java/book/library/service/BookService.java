package book.library.service;

import java.util.List;
import java.util.Set;

import book.library.model.Book;

public interface BookService {

    void loadBooks();

    Book findByIsbn(String isbn);

    Set<Book> findByAuthor(String author);

    List<Book> findAllBooksSortByGermanTitle();

    List<Book> getBooks();
}
