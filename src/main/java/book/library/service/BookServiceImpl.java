package book.library.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import book.library.model.Book;
import com.ibm.icu.text.CharsetDetector;
import com.ibm.icu.text.CharsetMatch;
import com.univocity.parsers.common.processor.BeanListProcessor;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.text.Collator.getInstance;
import static java.util.Collections.emptyList;
import static java.util.Comparator.comparing;
import static java.util.Locale.GERMAN;
import static java.util.stream.Collectors.toList;

public class BookServiceImpl implements BookService {

    private List<Book> books = emptyList();

    @Override
    public void loadBooks() {
        String charset = null;
        final String fileName = "books.csv";

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(fileName)) {
            final CharsetDetector charsetDetector = new CharsetDetector();
            charsetDetector.setText(inputStream);
            final CharsetMatch charsetMatch = charsetDetector.detect();
            charset = charsetMatch != null ? charsetMatch.getName() : UTF_8.name();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        final BeanListProcessor<Book> rowProcessor = new BeanListProcessor<>(Book.class);
        final CsvParserSettings parserSettings = new CsvParserSettings();
        parserSettings.setProcessor(rowProcessor);
        parserSettings.setHeaderExtractionEnabled(true);
        parserSettings.setDelimiterDetectionEnabled(true, ';');
        final CsvParser parser = new CsvParser(parserSettings);
        parser.parse(getClass().getClassLoader().getResourceAsStream(fileName), charset);

        // The BeanListProcessor provides a list of objects extracted from the input.
        books = rowProcessor.getBeans();
    }

    @Override
    public List<Book> getBooks() {
        return books;
    }

    @Override
    public Book findByIsbn(final String isbn) {
        return books.stream().filter(book -> book.getIsbn().equals(isbn)).findFirst().orElse(null);
    }

    @Override
    public Set<Book> findByAuthor(final String author) {
        final Set<Book> foundBooks = new HashSet<>();

        for (Book book : books) {
            for (String authorInABook : book.getAuthors()) {
                if (authorInABook.equals(author)) {
                    foundBooks.add(book);
                }
            }
        }

        return foundBooks;
    }

    @Override
    public List<Book> findAllBooksSortByGermanTitle() {
        return books.stream().sorted(comparing(Book::getTitle, getInstance(GERMAN))).collect(toList());
    }
}
