package book.library.service;

import java.util.List;
import java.util.Set;

import book.library.model.Magazine;

public interface MagazineService {

    void loadMagazines();

    Set<Magazine> findByIsbn(String isbn);

    Set<Magazine> findByAuthor(String author);

    List<Magazine> findAllMagazinesSortByGermanTitle();

    List<Magazine> geMagazines();
}
