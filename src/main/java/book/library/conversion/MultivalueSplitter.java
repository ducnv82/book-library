package book.library.conversion;

import com.univocity.parsers.conversions.Conversion;

public class MultivalueSplitter implements Conversion<String, String[]> {

    private String separator;

    public MultivalueSplitter(final String... args) {
        if (args.length == 0) {
            separator = ",";
        } else {
            separator = args[0];
        }
    }

    @Override
    public String[] execute(final String input) {
        if (input == null) {
            return new String[0];
        }
        return input.split(separator);
    }

    @Override
    public String revert(final String[] input) {
        final StringBuilder out = new StringBuilder();
        for (String value : input) {
            if (out.length() > 0) {
                out.append(separator);
            }
            out.append(value);
        }
        return out.toString();
    }
}
