package book.library.model;

import java.util.Arrays;
import java.util.Objects;

import book.library.conversion.MultivalueSplitter;
import book.library.util.Utils;
import com.univocity.parsers.annotations.Convert;
import com.univocity.parsers.annotations.Parsed;

/**
 * Book model.
 *
 * @author duc
 */
public class Book {

    @Parsed(field = "title")
    private String title;

    @Parsed(field = "isbn")
    private String isbn;

    @Parsed(field = "description")
    private String description;

    @Parsed(field = "authors")
    @Convert(conversionClass = MultivalueSplitter.class, args = ",")
    private String [] authors;

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(final String isbn) {
        this.isbn = isbn;
    }

    public String[] getAuthors() {
        return authors;
    }

    public void setAuthors(final String[] authors) {
        this.authors = authors;
    }

    @Override
    public String toString() {
        return Utils.GSON.toJson(this);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, isbn, description, Arrays.hashCode(authors));
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof Book)) {
            return false;
        }

        final Book book = (Book) o;

        return Objects.equals(title, book.title) &&
               Objects.equals(isbn, book.isbn) &&
               Objects.equals(description, book.description) &&
               Arrays.equals(authors, book.authors);
    }
}
