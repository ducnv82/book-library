package book.library.model;

import com.univocity.parsers.annotations.Parsed;

import static book.library.util.Utils.GSON;

/**
 * Author model.
 *
 * @author duc
 */
public class Author {

    @Parsed(field = "email")
    private String email;

    @Parsed(field = "firstname")
    private String firstName;

    @Parsed(field = "lastname")
    private String lastName;

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return GSON.toJson(this);
    }
}
