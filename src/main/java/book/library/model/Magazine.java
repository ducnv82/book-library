package book.library.model;

import java.util.Arrays;
import java.util.Objects;

import book.library.conversion.MultivalueSplitter;
import book.library.util.Utils;
import com.univocity.parsers.annotations.Convert;
import com.univocity.parsers.annotations.Parsed;

/**
 * Magazine model.
 *
 * @author duc
 */
public class Magazine {

    @Parsed(field = "title")
    private String title;

    @Parsed(field = "isbn")
    private String isbn;

    @Parsed(field = "author")
    @Convert(conversionClass = MultivalueSplitter.class, args = ",")
    private String [] authors;

    @Parsed(field = "publicationdate")
    private String publicationDate;

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(final String isbn) {
        this.isbn = isbn;
    }

    public String getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(final String publicationDate) {
        this.publicationDate = publicationDate;
    }

    public String[] getAuthors() {
        return authors;
    }

    public void setAuthors(final String[] authors) {
        this.authors = authors;
    }

    @Override
    public String toString() {
        return Utils.GSON.toJson(this);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, isbn, publicationDate, Arrays.hashCode(authors));
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof Magazine)) {
            return false;
        }

        final Magazine magazine = (Magazine) o;

        return Objects.equals(title, magazine.title) &&
               Objects.equals(isbn, magazine.isbn) &&
               Objects.equals(publicationDate, magazine.publicationDate) &&
               Arrays.equals(authors, magazine.authors);
    }
}
