package book.library;

import book.library.service.AuthorService;
import book.library.service.AuthorServiceImpl;
import book.library.service.BookService;
import book.library.service.BookServiceImpl;
import book.library.service.MagazineService;
import book.library.service.MagazineServiceImpl;

public class BookLibraryApplication {

    public static void main(final String[] args) {
        final AuthorService authorService = new AuthorServiceImpl();
        final BookService bookService = new BookServiceImpl();
        final MagazineService magazineService = new MagazineServiceImpl();

        authorService.loadAuthors();
        bookService.loadBooks();
        magazineService.loadMagazines();

        System.out.println("Print out all details of all books:");
        bookService.getBooks().forEach(System.out::println);
        System.out.println("\nPrint out all details of all magazines:");
        magazineService.geMagazines().forEach(System.out::println);

        System.out.println("\nPrint out all details of a book by searching with an ISBN:");
        System.out.println(bookService.findByIsbn("2215-0012-5487"));
        System.out.println("\nPrint out all details of all magazines by searching with an ISBN:");
        System.out.println(magazineService.findByIsbn("4545-8541-2012"));

        System.out.println("\nFind and print out the details of all books for an author:");
        System.out.println(bookService.findByAuthor("pr-walter@optivo.de"));

        System.out.println("\nFind and print out the details of all magazines for an author:");
        System.out.println(magazineService.findByAuthor("pr-walter@optivo.de"));

        System.out.println("\nSort all books by title:");
        System.out.println(bookService.findAllBooksSortByGermanTitle());

        System.out.println("\nSort all magazines by title:");
        System.out.println(magazineService.findAllMagazinesSortByGermanTitle());
    }
}
