package book.library.service;

import java.util.List;

import book.library.model.Author;
import org.junit.Before;
import org.junit.Test;

import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertTrue;

public class AuthorServiceImplTest {

    private AuthorService authorService = new AuthorServiceImpl();

    @Before
    public void setUp() {
        authorService.loadAuthors();
    }

    @Test
    public void testLoadAuthors_notEmpty() {
        assertTrue(authorService.getAuthors().size() == 6);
    }

    @Test
    public void testLoadAuthors_email() {
        final List<String> authorEmails = authorService.getAuthors().stream().map(Author::getEmail).collect(toList());

        assertTrue(authorEmails.contains("pr-walter@optivo.de"));
        assertTrue(authorEmails.contains("pr-mueller@optivo.de"));
        assertTrue(authorEmails.contains("pr-ferdinand@optivo.de"));
        assertTrue(authorEmails.contains("pr-gustafsson@optivo.de"));
        assertTrue(authorEmails.contains("pr-lieblich@optivo.de"));
        assertTrue(authorEmails.contains("pr-rabe@optivo.de"));
    }

    @Test
    public void testLoadAuthors_firstName() {
        final List<String> firstNames = authorService.getAuthors().stream().map(Author::getFirstName).collect(toList());

        assertTrue(firstNames.contains("Paul"));
        assertTrue(firstNames.contains("Max"));
        assertTrue(firstNames.contains("Franz"));
        assertTrue(firstNames.contains("Karl"));
        assertTrue(firstNames.contains("Werner"));
        assertTrue(firstNames.contains("Harald"));
    }

    @Test
    public void testLoadAuthors_lastName() {
        final List<String> lastNames = authorService.getAuthors().stream().map(Author::getLastName).collect(toList());
        assertTrue(lastNames.contains("Walter"));
        assertTrue(lastNames.contains("Müller"));
        assertTrue(lastNames.contains("Ferdinand"));
        assertTrue(lastNames.contains("Gustafsson"));
        assertTrue(lastNames.contains("Lieblich"));
        assertTrue(lastNames.contains("Rabe"));
    }
}
