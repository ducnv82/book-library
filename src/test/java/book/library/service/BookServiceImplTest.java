package book.library.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import book.library.model.Book;
import org.junit.Before;
import org.junit.Test;

import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class BookServiceImplTest {

    private BookService bookService = new BookServiceImpl();

    @Before
    public void setUp() {
        bookService.loadBooks();
    }

    @Test
    public void testLoadBooks_notEmpty() {
        assertTrue(bookService.getBooks().size() == 8);
    }

    @Test
    public void testLoadBooks_title() {
        final List<String> titles = bookService.getBooks().stream().map(Book::getTitle).collect(toList());

        assertTrue(titles.contains("Ich helf dir kochen. Das erfolgreiche Universalkochbuch mit großem Backteil"));
        assertTrue(titles.contains("Das große GU-Kochbuch Kochen für Kinder"));
        assertTrue(titles.contains("Schlank im Schlaf"));
        assertTrue(titles.contains("Das Perfekte Dinner. Die besten Rezepte"));
        assertTrue(titles.contains("Das Piratenkochbuch. Ein Spezialitätenkochbuch mit den 150 leckersten Rezepten"));
        assertTrue(titles.contains("Genial italienisch"));
        assertTrue(titles.contains("O'Reillys Kochbuch für Geeks"));
        assertTrue(titles.contains("Schuhbecks Kochschule. Kochen lernen mit Alfons Schuhbeck"));
    }

    @Test
    public void testLoadBooks_isbn() {
        final List<String> isbns = bookService.getBooks().stream().map(Book::getIsbn).collect(toList());

        assertTrue(isbns.contains("5554-5545-4518"));
        assertTrue(isbns.contains("2145-8548-3325"));
        assertTrue(isbns.contains("4545-8558-3232"));
        assertTrue(isbns.contains("2221-5548-8585"));
        assertTrue(isbns.contains("3214-5698-7412"));
        assertTrue(isbns.contains("1024-5245-8584"));
        assertTrue(isbns.contains("2215-0012-5487"));
        assertTrue(isbns.contains("1215-4545-5895"));
    }

    @Test
    public void testLoadBooks_authors() {
        final List<String> allAuthors = new ArrayList<>();
        bookService.getBooks().stream().map(Book::getAuthors).collect(toList()).forEach(authorsArray -> {
            Arrays.stream(authorsArray).forEach(author -> allAuthors.add(author));
        });

        assertTrue(allAuthors.contains("pr-walter@optivo.de"));

        assertTrue(allAuthors.contains("pr-ferdinand@optivo.de"));
        assertTrue(allAuthors.contains("pr-lieblich@optivo.de"));

        assertTrue(allAuthors.contains("pr-gustafsson@optivo.de"));
        assertTrue(allAuthors.contains("pr-lieblich@optivo.de"));
        assertTrue(allAuthors.contains("pr-rabe@optivo.de"));
        assertTrue(allAuthors.contains("pr-rabe@optivo.de"));
        assertTrue(allAuthors.contains("pr-mueller@optivo.de"));
        assertTrue(allAuthors.contains("pr-walter@optivo.de"));
    }

    @Test
    public void testLoadBooks_2authors_byISBN() {
        final List<String> twoAuthors = new ArrayList<>();
        bookService.getBooks().stream().filter(book -> "2145-8548-3325".equals(book.getIsbn())).map(Book::getAuthors).collect(toList()).forEach(authorsArray -> {
            Arrays.stream(authorsArray).forEach(author -> twoAuthors.add(author));
        });

        assertTrue(twoAuthors.size() == 2);
        assertTrue(twoAuthors.contains("pr-ferdinand@optivo.de"));
        assertTrue(twoAuthors.contains("pr-lieblich@optivo.de"));
    }

    @Test
    public void testLoadBooks_3authors_byISBN() {
        final List<String> threeAuthors = new ArrayList<>();
        bookService.getBooks().stream().filter(book -> "1024-5245-8584".equals(book.getIsbn())).map(Book::getAuthors).collect(toList()).forEach(authorsArray -> {
            Arrays.stream(authorsArray).forEach(author -> threeAuthors.add(author));
        });

        assertTrue(threeAuthors.size() == 3);
        assertTrue(threeAuthors.contains("pr-lieblich@optivo.de"));
        assertTrue(threeAuthors.contains("pr-walter@optivo.de"));
        assertTrue(threeAuthors.contains("pr-rabe@optivo.de"));
    }

    @Test
    public void testLoadBooks_description() {
        final List<String> descriptions = bookService.getBooks().stream().map(Book::getDescription).collect(toList());

        assertTrue(descriptions.contains("Auf der Suche nach einem Basiskochbuch steht man heutzutage vor einer Fülle von Alternativen. Es fällt schwer, daraus die für sich passende Mixtur aus Grundlagenwerk und Rezeptesammlung zu finden. Man sollte sich darüber im Klaren sein, welchen Schwerpunkt man setzen möchte oder von welchen Koch- und Backkenntnissen man bereits ausgehen kann."));
        assertTrue(descriptions.contains("Es beginnt mit... den ersten Löffelchen für Mami, Papi und den Rest der Welt. Ja, und dann? Was Hersteller von Babynahrung können, das ist oft im Handumdrehen auch selbst gemacht, vielleicht sogar gesünder, oftmals frischer. Ältere Babys und Schulkinder will das Kochbuch ansprechen und das tut es auf eine verspielte Art angenehm altersgemäß."));
        assertTrue(descriptions.contains("Schlank im Schlaf klingt wie ein schöner Traum, aber es ist wirklich möglich. Allerdings nicht nach einer Salamipizza zum Abendbrot. Die Grundlagen dieses neuartigen Konzepts sind eine typgerechte Insulin-Trennkost sowie Essen und Sport im Takt der biologischen Uhr. Wie die Bio-Uhr tickt und was auf dem Speiseplan stehen sollte, hängt vom persönlichen Urtyp ab: Nomade oder Ackerbauer?"));
        assertTrue(descriptions.contains("Sie wollen auch ein perfektes Dinner kreieren? Mit diesem Buch gelingt es Ihnen!"));
        assertTrue(descriptions.contains("Das Piraten-Kochbuch beweist, dass die Seeräuberkapitäne zwar gefürchtete Haudegen waren, andererseits aber manches Mal mit gehobenenem Geschmacksempfinden ausgestattet. ... Kurzum, ein ideales Buch, um maritime Events kulinarisch zu umrahmen."));
        assertTrue(descriptions.contains("Starkoch Jamie Oliver war mit seinem VW-Bus in Italien unterwegs -- und hat allerlei kulinarische Souvenirs mitgebracht. Es lohnt sich, einen Blick in sein Gepäck zu werfen..."));
        assertTrue(descriptions.contains("Nach landläufiger Meinung leben Geeks von Cola und TK-Pizza, die sie nachts am Rechner geistesabwesend in sich reinschlingen. Soweit das Klischee! Dass aber Kochen viel mit Programmieren zu tun hat, dass es nämlich ähnlich kreativ ist, dass viele Wege zum individuellen Ziel führen und dass manche Rezepte einfach nur abgefahren und -- ja, geekig sind: Das zeigen zwei Köchinnen in diesem Buch."));
        assertTrue(descriptions.contains("Aller Anfang ist leicht! Zumindest, wenn man beim Kochenlernen einen Lehrer wie Alfons Schuhbeck zur Seite hat. Mit seiner Hilfe kann auch der ungeschickteste Anfänger beste Noten für seine Gerichte bekommen. Der Trick, den der vielfach ausgezeichnete Meisterkoch dabei anwendet, lautet visualisieren. Die einzelnen Arbeitsschritte werden auf Farbfotos im Format von ca. 3x4 cm abgebildet. Unter diesen stehen kurz und knapp die Angaben zur Zubereitung. So präsentiert Schuhbecks Kochschule alles bequem auf einen Blick. Und der interessierte Kochneuling kann sich auf die Hauptsache konzentrieren – aufs Kochen. Wie die Speise aussehen soll, zeigt jeweils das Farbfoto auf der linken Seite, auf der auch die Zutaten – dank des geschickten Layouts – ebenfalls sehr übersichtlich aufgelistet sind. Außerdem gibt Schuhbeck prägnante Empfehlungen zu Zutaten und Zubereitung."));
    }

    @Test
    public void testFindByIsbn() {
        final Book book = bookService.findByIsbn("5554-5545-4518");
        assertNotNull(book);
    }

    @Test
    public void testFindByIsbn_notFound() {
        final Book book = bookService.findByIsbn("NotFoundISBN");
        assertNull(book);
    }

    @Test
    public void testFindByIsbn_isbnNull() {
        final Book book = bookService.findByIsbn(null);
        assertNull(book);
    }

    @Test
    public void testFindByAuthor() {
        final Set<Book> foundBooks = bookService.findByAuthor("pr-walter@optivo.de");
        assertTrue(foundBooks.size() == 3);
    }

    @Test
    public void testFindByAuthor_notFound() {
        final Set<Book> foundBooks = bookService.findByAuthor("NotFoundAuthor");
        assertTrue(foundBooks.size() == 0);
    }

    @Test
    public void testFindByAuthor_authorNull() {
        final Set<Book> foundBooks = bookService.findByAuthor(null);
        assertTrue(foundBooks.size() == 0);
    }

    @Test
    public void testFindAllBooksSortByGermanTitle() {
        final List<Book> books = bookService.findAllBooksSortByGermanTitle();

        assertTrue(books.get(0).getTitle().startsWith("Das große GU-Kochbuch Kochen für Kinder"));
        assertTrue(books.get(1).getTitle().startsWith("Das Perfekte Dinner. Die besten Rezepte"));
        assertTrue(books.get(2).getTitle().startsWith("Das Piratenkochbuch. Ein Spezialitätenkochbuch mit den 150 leckersten Rezepten"));
        assertTrue(books.get(3).getTitle().startsWith("Genial italienisch"));
        assertTrue(books.get(4).getTitle().startsWith("Ich helf dir kochen. Das erfolgreiche Universalkochbuch mit großem Backteil"));
        assertTrue(books.get(5).getTitle().startsWith("O'Reillys Kochbuch für Geeks"));
        assertTrue(books.get(6).getTitle().startsWith("Schlank im Schlaf"));
        assertTrue(books.get(7).getTitle().startsWith("Schuhbecks Kochschule. Kochen lernen mit Alfons Schuhbeck"));
    }
}
