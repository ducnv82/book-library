package book.library.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import book.library.model.Magazine;
import org.junit.Before;
import org.junit.Test;

import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertTrue;

public class MagazineServiceImplTest {

    private MagazineService magazineService = new MagazineServiceImpl();

    @Before
    public void setUp() {
        magazineService.loadMagazines();
    }

    @Test
    public void testLoaMagazines_notEmpty() {
        assertTrue(magazineService.geMagazines().size() == 6);
    }

    @Test
    public void testLoadMagazines_title() {
        final List<String> titles = magazineService.geMagazines().stream().map(Magazine::getTitle).collect(toList());

        assertTrue(titles.contains("Schöner kochen"));
        assertTrue(titles.contains("Meine Familie und ich"));
        assertTrue(titles.contains("Kochen für Genießer"));
        assertTrue(titles.contains("Gourmet"));
        assertTrue(titles.contains("Der Weinkenner"));
        assertTrue(titles.contains("Vinum"));
    }

    @Test
    public void testLoadMagazines_isbn() {
        final List<String> isbns = magazineService.geMagazines().stream().map(Magazine::getIsbn).collect(toList());

        assertTrue(isbns.contains("5454-5587-3210"));
        assertTrue(isbns.contains("4545-8541-2012"));
        assertTrue(isbns.contains("2365-5632-7854"));
        assertTrue(isbns.contains("2365-8745-7854"));
        assertTrue(isbns.contains("2547-8548-2541"));
        assertTrue(isbns.contains("1313-4545-8875"));
    }

    @Test
    public void testLoadMagazines_publicationdate() {
        final List<String> publicationDate = magazineService.geMagazines().stream().map(Magazine::getPublicationDate).collect(toList());

        assertTrue(publicationDate.contains("21.05.2006"));
        assertTrue(publicationDate.contains("10.07.2006"));
        assertTrue(publicationDate.contains("01.05.2007"));
        assertTrue(publicationDate.contains("14.06.2005"));
        assertTrue(publicationDate.contains("12.12.2002"));
        assertTrue(publicationDate.contains("23.02.2004"));
    }

    @Test
    public void testLoadBooks_authors() {
        final List<String> allAuthors = new ArrayList<>();
        magazineService.geMagazines().stream().map(Magazine::getAuthors).collect(toList()).forEach(authorsArray -> {
            Arrays.stream(authorsArray).forEach(author -> allAuthors.add(author));
        });

        assertTrue(allAuthors.contains("pr-walter@optivo.de"));
        assertTrue(allAuthors.contains("pr-mueller@optivo.de"));
        assertTrue(allAuthors.contains("pr-lieblich@optivo.de"));
        assertTrue(allAuthors.contains("pr-walter@optivo.de"));
        assertTrue(allAuthors.contains("pr-ferdinand@optivo.de"));
        assertTrue(allAuthors.contains("pr-walter@optivo.de"));
        assertTrue(allAuthors.contains("pr-mueller@optivo.de"));
        assertTrue(allAuthors.contains("pr-gustafsson@optivo.de"));
    }

    @Test
    public void testLoadBooks_2authors_byISBN() {
        final List<String> twoAuthors = new ArrayList<>();
        magazineService.geMagazines().stream().filter(magazine -> "2365-5632-7854".equals(magazine.getIsbn())).map(Magazine::getAuthors).collect(toList()).forEach(authorsArray -> {
            Arrays.stream(authorsArray).forEach(author -> twoAuthors.add(author));
        });

        assertTrue(twoAuthors.size() == 2);
        assertTrue(twoAuthors.contains("pr-lieblich@optivo.de"));
        assertTrue(twoAuthors.contains("pr-walter@optivo.de"));
    }

    @Test
    public void testFindByIsbn() {
        final Set<Magazine> foundMagazines = magazineService.findByIsbn("5454-5587-3210");
        assertTrue(foundMagazines.size() == 1);
    }

    @Test
    public void testFindByIsbn_notFound() {
        final Set<Magazine> foundMagazines = magazineService.findByIsbn("NotFoundISBN");
        assertTrue(foundMagazines.size() == 0);
    }

    @Test
    public void testFindByIsbn_isbnNull() {
        final Set<Magazine> foundMagazines = magazineService.findByIsbn("NotFoundISBN");
        assertTrue(foundMagazines.size() == 0);
    }

    @Test
    public void testFindByAuthor() {
        final Set<Magazine> foundMagazines = magazineService.findByAuthor("pr-walter@optivo.de");
        assertTrue(foundMagazines.size() == 3);
    }

    @Test
    public void testFindByAuthor_notFound() {
        final Set<Magazine> foundMagazines = magazineService.findByAuthor("NotFoundAuthor");
        assertTrue(foundMagazines.size() == 0);
    }

    @Test
    public void testFindByAuthor_authorNull() {
        final Set<Magazine> foundMagazines = magazineService.findByAuthor(null);
        assertTrue(foundMagazines.size() == 0);
    }

    @Test
    public void testFindAllMagazinesSortByGermanTitle() {
        final List<Magazine> magazines = magazineService.findAllMagazinesSortByGermanTitle();

        assertTrue(magazines.get(0).getTitle().startsWith("Der Weinkenner"));
        assertTrue(magazines.get(1).getTitle().startsWith("Gourmet"));
        assertTrue(magazines.get(2).getTitle().startsWith("Kochen für Genießer"));
        assertTrue(magazines.get(3).getTitle().startsWith("Meine Familie und ich"));
        assertTrue(magazines.get(4).getTitle().startsWith("Schöner kochen"));
        assertTrue(magazines.get(5).getTitle().startsWith("Vinum"));
    }
}
