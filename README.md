# Requirements
You should implement a book library containing the following data:

*   There are different authors having an email address, first name and surname
*   There are regular books having a title, a short description, one or more authors and an ISBN
*   There are magazines having a title, one or more authors, a publish date and an ISBN

The following functionalities should be provided for the managed books and magazines:

*   Reading all data from all the CSV files in the folder 'data'. You'll find them in the attached ZIP archive. The structure of the files should be self-explaining.
*   Print out all details of all books and magazines
*   Find and print out the details of a book or magazine by searching with an ISBN
*   Find and print out the details of a book or magazine for an author
*   Sort all books and magazine by title and print out the result

# Run the application

1. Installations: Install latest Oracle JDK 8, download at http://www.oracle.com/technetwork/java/javase/downloads/index.html
2. Download and install latest Gradle (or at least Gradle 4.9)
3. In the root of the application type `gradle run` to run the app.
4. In the root of the application type `gradle test` to run the unit tests.


# Thought about requirements in QA perspective
1. Find and print out the details of a book or magazine by searching with an ISBN: only books have ISBN, **[magazine does not have ISBN](https://www.myidentifiers.com/help/isbn "link") ** so the requirement for **magazine** should be "Find and print out the details of **all magazines** by searching with an ISBN".
2. Find and print out the details of a book or magazine for an author: **an author can have many books or many magazines** so the requirement should be "Find and print out the details of **all books** or **all magazines** for an author".  

Feel free to discuss the changes (if needed) for requirements.
